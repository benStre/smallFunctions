def fib(n):
	global c; c+=1
	return fib(n-1)+fib(n-2) if n>2 else (0 if n==0 else 1)

fibonacci = lambda n: fibonacci(n-1)+fibonacci(n-2) if n>2 else (0 if n==0 else 1)

while True:
	c = 0
	try:
		n = input("Calculate a fibonacci number: n=")
		print("it's " + str(fibonacci(int(n))))
		print("...just " + str(c) + " recursions")
	except:
		print("n needs to be an integer")
		
